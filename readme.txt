=== S3 Image Optimizer ===
Contributors: nosilver4u
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=MKMQKCBFFG3WW
Tags: amazone, s3, cloudfront, image, optimize, optimization, photo, picture, seo, compression, wp-cli 
Requires at least: 3.8
Tested up to: 4.4
Stable tag: .1
License: GPLv3

Reduce file sizes for images in S3 buckets using lossless and lossy optimization methods via the EWWW Image Optimizer.

== Description ==

The S3 Image Optimizer is a WordPress plugin that will allow you to retrieve all the images within one or more Amazon S3 buckets and optimize them using the EWWW Image Optimizer. It also requires the Amazon Web Services plugin to work with Amazon S3.

It currently uses a web-based optimization process, but a wp-cli interface is on the roadmap, as well as a hosted version dedicated exclusively to optimizing S3 buckets.

== Installation ==

1. Install and configure the EWWW Image Optimizer plugin.
1. Install the [Amazon Web Services plugin](http://wordpress.org/extend/plugins/amazon-web-services/) using WordPress' built-in installer.
1. Follow the instructions to setup your AWS access keys.
1. Install this plugin via Wordpress' built-in plugin installer.
1. Enter S3 bucket names under Settings and S3 Image Optimizer.
1. Go to Media and S3 Image Optimizer to start optimizing your bucket.

== Frequently Asked Questions ==

= Why aren't there any questions here? =

Start asking, and then we'll see what needs answering: https://wordpress.org/support/plugin/s3-image-optimizer

== Changelog ==

= .1 =
* First release
